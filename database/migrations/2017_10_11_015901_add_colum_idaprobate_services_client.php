<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumIdaprobateServicesClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services_client', function (Blueprint $table) {
            $table->integer('idaprobate');   //por defecto tipo usuario 1 (cliente), para no enviarlo desde ionic, ya que ahí solo se registrarán clientes.          
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('services_client', function (Blueprint $table) {
             $table->dropcolumn('idaprobate');            
        });
    }
}
