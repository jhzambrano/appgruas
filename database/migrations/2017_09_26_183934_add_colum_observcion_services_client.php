<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumObservcionServicesClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('services_client', function (Blueprint $table) {
            $table->string('observacion',50);            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('services_client', function (Blueprint $table) {
             $table->dropcolumn('observacion');            
        });
    }
}
