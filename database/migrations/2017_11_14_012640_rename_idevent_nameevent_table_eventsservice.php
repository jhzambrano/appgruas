<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameIdeventNameeventTableEventsservice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('eventsservice', function($t) {
                        $t->renameColumn('idevent', 'event_name');
                });
    }


    public function down()
    {
        Schema::table('eventsservice', function($t) {
                        $t->renameColumn('event_name', 'idevent');
                });
    }
}
