<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsLatlongServicesclient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('services_client', function (Blueprint $table) {
            $table->string('lat',20);            
            $table->string('long',20);            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('services_client', function (Blueprint $table) {
             $table->dropcolumn('lat');            
            $table->dropcolumn('long'); 

        });
    }
}
