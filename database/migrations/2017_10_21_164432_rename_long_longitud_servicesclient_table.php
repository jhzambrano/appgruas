<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameLongLongitudServicesclientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services_client', function($t) {
                        $t->renameColumn('long', 'longitud');
                });
    }


    public function down()
    {
        Schema::table('services_client', function($t) {
                        $t->renameColumn('longitud', 'long');
                });
    }
}
