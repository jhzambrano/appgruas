<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMovservice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
         Schema::create('movservice', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_service');                      
            $table->integer('fase');                      
            $table->dateTime('datestaend');
            $table->dateTime('timestaend');            
            $table->string('latitud');
            $table->string('longitud');
            $table->integer('startorend'); //strart=1, end=2
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::dropIfExists('movservice');
    }
}
