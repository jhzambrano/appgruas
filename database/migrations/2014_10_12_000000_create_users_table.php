<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {                        
            $table->rememberToken();
            $table->timestamps();
            $table->string('business');
            $table->string('telephone');
            $table->string('address');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('users', function (Blueprint $table) {             
             $table->dropcolumn('business');
            $table->dropcolumn('telephone');
            $table->dropcolumn('address');      
        });  
    }
}
