<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameEmailToEmailUserstable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('users', function($t) {
                        $t->renameColumn('Email', 'email');
                });
    }


    public function down()
    {
        Schema::table('users', function($t) {
                        $t->renameColumn('email', 'Email');
                });
    }
}
