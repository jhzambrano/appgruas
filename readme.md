##Instalación de Laravel
Debemos tener una serie de aplicaciones instaladas para poder ejecutar nuestros proyectos

- [Servidor apache y mysql (XAMPP Recomendado)](https://www.apachefriends.org/es/index.html).
- Instalar composer (Windows)](https://getcomposer.org/download/).
- Instalar composer (Linux)]
	-Instalar Dependencias: sudo apt-get update
	sudo apt-get install curl php5-cli git
	curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

NOTA: Otra guia para instalar los paquetes de composer en laravel y linux se puede encontrar en:
https://styde.net/instalacion-de-composer-y-laravel-en-windows/


La forma de verificar si está instalado al menos en windows es abriendo la terminal y escribiendo el comando composer. Allí deberá mostrar la instalación del paquete	

##Crear una aplicación en laravel


en la terminal dentro del directorio donde se quiere crear el proyecto debes escribir:

composer create-project laravel/laravel nombre_proyecto Ejm:

composer create-project laravel/laravel mi_primer_proyecto

NOTA: Cuando no se agrega la versión, por defecto laravel crea un esquema para el proyecto con la última versión activa, pero se puede especificar
una versión en particular para nuestro proyecto. Para ello se indica así:

composer create-project laravel/laravel nombre_proyecto 4.2 -prefer-dist
Esto crea un proyecto para la versión 4.2

Nota: La documentación del framework puede encontrarla en:
https://laravel.com/docs/5.5

5.5 es la versión actual de laravel

##Configurar proyecto laravel para interacción con Base de Datos.

En la raíz del proyecto se crea un archivo con el nombre .env, el cual presenta una configuración como esta:

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE="prueba" 
DB_USERNAME=root
DB_PASSWORD=''

Esta configuración indica que se va a conectar  a la base de datos "prueba" con el usuario root y sin contraseña. El usuario deberá configurar estos parametros
de acuerdo al nivel de seguridad que tenga en mysql


##obtener las ultimas versiones de las dependencias

inicie la terminal, ubiquese en el directorio de su proyecto y escriba:
php composer.phar update

Esto actualiza las versiones de las dependencias y las modifica en el archivo composer.lock

##passport

Es una librería que permite autenticación y seguridad mediante una API.

La documentación necesaria para instalarla puede encontrarla en:

https://laravel.com/docs/5.5/passport