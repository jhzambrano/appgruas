<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;


class ServicioController extends Controller
{    
	function eliminaservicio($id)
    {
        $id=$_POST['id'];
        //se busca ese id en la tabla services_client
        $sql="select * from services_client where servicetype=$id";
        $resultado = \DB::select($sql); 
        $data=array();
        if(count($resultado)>0) //hay xclientes con ese servicio
        {
            $data=array(
                "puedeeliminar"=>"no"
            );
        }
        else
        {
            //se manada a elimilar
           $sql="delete from services where id=$id";
             $resultado = \DB::select($sql); 
             $data=array(
                "puedeeliminar"=>$sql
            );
             $data=array(
                "puedeeliminar"=>"si"
            );
        }
        return json_encode($data);
        //$sql="delete from "
    }
    function eliminarevento($id)
    {
        $id=$_POST['id'];
        //se busca ese id en la tabla services_client
       
            //se manada a elimilar
           $sql="delete from events where id=$id";
             $resultado = \DB::select($sql); 
             
             $data=array(
                "puedeeliminar"=>"si"
            );
       
        return json_encode($data);
        //$sql="delete from "
    }
    function eliminarmotivo($id)
    {
        $id=$_POST['id'];
        //se busca ese id en la tabla services_client
       
            //se manada a elimilar
           $sql="delete from motivoscancelacion where id=$id";
             $resultado = \DB::select($sql); 
             
             $data=array(
                "puedeeliminar"=>"si"
            );
       
        return json_encode($data);
        //$sql="delete from "
    }
    function regservicios(Request $request)
    {

         $description=$_GET['description'];
         $data=array(
            "description"=> $description        
        );    
        \DB::table('services')->insert($data);
   //se redirecciona a home
        $data2=array(
            "registrado"=>"El Servicio se registró con Éxito..."
        );
        return json_encode($data2);
 
    }
    function regevento(Request $request)
    {

         $description=$_GET['description'];
         $data=array(
            "description"=> $description        
        );    
        \DB::table('events')->insert($data);
   //se redirecciona a home
        $data2=array(
            "registrado"=>"El Motivose registró con Éxito..."
        );
        return json_encode($data2);
 
    }
    function regmotivos(Request $request)
    {

         $description=$_GET['description'];
         $data=array(
            "description"=> $description        
        );    
        \DB::table('motivoscancelacion')->insert($data);
   //se redirecciona a home
        $data2=array(
            "registrado"=>"El Servicio se registró con Éxito..."
        );
        return json_encode($data2);
 
    }
    function editarservicio($id,Request $request){

       
        $id_servicio=$_POST['id'];
        $description=$_POST['description'];
        $sql="update services set description='".$description."' where id=$id_servicio";
        $resultado = \DB::select($sql);      
     //$description=$request->get('id_servicio');
        $data=array(
            "respuesta"=>"Servicio Editado Exitosamente..."
        );
        //print_r($data);*/
       return json_encode($data);
    }
    function editarevento($id,Request $request){


        $id_evento=$_POST['id'];
        $description=$_POST['description'];
        $sql="update events set description='".$description."' where id=$id_evento";
        $resultado = \DB::select($sql);      
     //$description=$request->get('id_servicio');
        $data=array(
            "respuesta"=>"Motivo del Evento Editado Exitosamente..."
        );
        //print_r($data);
       return json_encode($data);

    }
    function editarmotivo($id,Request $request){

       
        $id_servicio=$_POST['id'];
        $description=$_POST['description'];
        $sql="update motivoscancelacion set description='".$description."' where id=$id_servicio";
        $resultado = \DB::select($sql);      
     //$description=$request->get('id_servicio');
        $data=array(
            "respuesta"=>"Motivo de Cancelacion Editado Exitosamente..."
        );
        //print_r($data);
       return json_encode($data);

    }
    function serviciostodosclientes()
    {
        $sql="select users.Name, users.Last_name, users.id from users inner join 
        services_client on services_client.id_user=users.id and services_client.status_service=0 group by users.id";
          $resultado = \DB::select($sql);        
          //ahora se llena el arreglo con los datos y se pasa a la vista
          $data=array();      
         
            foreach ($resultado as $key) {
                array_push($data ,  array(
                "id_user"=>$key->id,
                "name"=>$key->Name,
                "Last_name"=>$key->Last_name            
                ));
            }                           
            if(count($resultado)>0)
            {
                //ahora se llama la vista y se pasa la información                 
                return View::make('services')->with('datos', $data);
            }    
    }
    function mostrarserviciosporcliente($id)
    {   
        
        $sql="select sc.startdate,sc.starttime,id_user,sc.id as id_service,sc.servicetype,s.description, u.Name, u.Last_name, u.token_api from users as u inner join services_client as sc inner join services as s on u.id=sc.id_user and sc.status_service=0 and s.id=sc.servicetype and  sc.id_user=$id order by id_service asc";
        $resultado = \DB::select($sql);    
       $sql2="select * from razones_aprobar";
       $resultado2=\DB::select($sql2);    
        $data=array();      
        foreach ($resultado as $key) {
            array_push($data ,  array(            
            "name"=>$key->Name,
            "token_api"=>$key->token_api,   
            "Last_name"=>$key->Last_name,
            "startdate"=>$key->startdate,
            "starttime"=>$key->starttime,
            "id_service"=>$key->id_service,
            "servicetype"=>$key->servicetype,
            "description"=>$key->description, 
            "razones"=>$resultado2                                  
            ));    
        }
        return json_encode($data);
    }
    function actualizarservicioscliente($id_servicio)
    {
        
        $sql="update services_client set status_service=1 where id=$id_servicio ";
        $resultado = \DB::select($sql);  
        $data=array(
            "guardado"=>"guardado"
        );
        return json_encode($data);
    }    

    function sendmsj(){
        $body = $_GET['body'];
        $title = $_GET['title'];
#API access key from Google API's Console
    define( 'API_ACCESS_KEY', 'AAAAMP3zBhM:APA91bHMs6Eil_O4gR4iSRqVg6m1oZ1Sxj5BmAW3bVScSc7iVIM5mxi4HUiS4zwNNtlOPDuK9TkHNuUJAkLlc5Fiz6xBiCSnuxQBU7UQNI7NlkI-y_11wj_wzSeKqSirzUQKgxvmFApa' );
    $registrationIds = $_GET['token_registration'];      
#prep the bundle
     $msg = array
          (
        'body'  => $body,
        'title' =>  $title,
                'icon'  => 'myicon',/*Default Icon*/
                'sound' => 'mySound'/*Default sound*/
          );
    $fields = array
            (
                'to'        => $registrationIds,
                'notification'  => $msg
            );
    
    
    $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );
#Send Reponse To FireBase Server    
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
#Echo Result Of FireBase Server
echo $result;
    }

function eventos()
{
    //aqui se llama la vista para eventos
   $sql="select * from events";
     $resultado = \DB::select($sql);        
      //ahora se llena el arreglo con los datos y se pasa a la vista
      $data=array();              
        foreach ($resultado as $key) {
            array_push($data ,  array(
            "id"=>$key->id,
            "description"=>$key->description
         
            ));
        }                       
    
    if(count($resultado)>0)
    {           
        return view('eventos')->with('servicios', $data);        
    }    

}
function aprobar(){
    if(count($_POST) > 0)
    {        
        
        foreach ($_POST["servicios"] as $item) {            
            \DB::update('update services_client set idoperador = "'.$_POST["typeuser"].'" WHERE id = "'.$item.'"');    
        }
    }
 
        $sql = "SELECT * FROM users WHERE typeuser = 2";
        $resultado = \DB::select($sql); 
        $data = array();
        foreach ($resultado as $key) {
            array_push($data ,  array(            
            "id"=>$key->id,
            "name"=>$key->Name." ".$key->Last_name,                                  
            ));    
        }


        $sql2 = "SELECT * FROM services_client WHERE status_service = 0 and idoperador=0";
        $resultado2 = \DB::select($sql2);         
        $data2 = array();
        foreach ($resultado2 as $key) {
            array_push($data2 ,  array(            
            "id"=>$key->id,  
            "servicetype"=>$key->servicetype,
            "observacion"=>$key->observacion                               
            ));    
        }


        return view('aprobar')->with('datos', $data)->with('datos2', $data2 );
    }
    

}
 