<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;


class ServicioController extends Controller
{    
	function serviciostodosclientes()
    {
        $sql="select users.Name, users.Last_name, users.id from users inner join 
        services_client on services_client.id_user=users.id and services_client.status_service=0 group by users.id";
          $resultado = \DB::select($sql);        
          //ahora se llena el arreglo con los datos y se pasa a la vista
          $data=array();      
            /*foreach ($resultado as $key) {
                array_push($data ,  array(
                "id_user"=>$key->id_user,
                "name"=>$key->name,
                "Last_name"=>$key->Last_name,
                "Email"=>$key->Email,
                "startdate"=>$key->startdate,
                "starttime"=>$key->starttime,
                "id_service"=>$key->id_service,
                "servicetype"=>$key->servicetype,
                "description"=>$key->description,                                   
                ));    
            }*/        
            foreach ($resultado as $key) {
                array_push($data ,  array(
                "id_user"=>$key->id,
                "name"=>$key->Name,
                "Last_name"=>$key->Last_name            
                ));
            }                           
            if(count($resultado)>0)
            {
                //ahora se llama la vista y se pasa la información                 
                return View::make('services')->with('datos', $data);
            }    
    }
    function mostrarserviciosporcliente($id)
    {   
        
        $sql="select sc.startdate,sc.starttime,id_user,sc.id as id_service,sc.servicetype,s.description, u.Name, u.Last_name, u.token_api from users as u inner join services_client as sc inner join services as s on u.id=sc.id_user and sc.status_service=0 and s.id=sc.servicetype and  sc.id_user=$id order by id_service asc";
        $resultado = \DB::select($sql);    
       
        $data=array();      
        foreach ($resultado as $key) {
            array_push($data ,  array(            
            "name"=>$key->Name,
            "Last_name"=>$key->Last_name,
            "startdate"=>$key->startdate,
            "starttime"=>$key->starttime,
            "id_service"=>$key->id_service,
            "servicetype"=>$key->servicetype,
            "description"=>$key->description,                                   
            "token_api"=>$key->token_api,                                   
            ));    
        }
        return json_encode($data);
    }
    function actualizarservicioscliente($id_servicio)
    {
        echo "aqui";
        $sql="update services_client set status_service=1 where id=$id_servicio ";
        $resultado = \DB::select($sql);  
        $data=array(
            "guardado"=>"guardado"
        );
        return json_encode($data);
    }
    /*function traerservicio()
    {
     
        $sql="select * from services ";
            $resultado = \DB::select( \DB::raw( $sql ));

            if( ( count($resultado)>0 ) ) //verifica que lleguen registros
            {
                //se crean las varaibles de session
                $data=array(
                        "id"=>$resultado->id,
                        "description"=>$resultado->description,
                        "exito"=>"si"
                );                
                
            }
            else
            {
                
                $data["exito"]="no";                

            }
            return json_decode($data);
       


        $sql="select * from services";
       
        $data=array(
            "nombre"=>'alexander',
            "apellido"=>'zambrano'
        )
        return json_decode($data);
     }*/
     /*function solicitarservicio(Request $request){	 	
	 	$data = array(
                'servicetype' => $request->input('servicetype'),
                'startdate' => $request->input('startdate'),
                'startime' => $request->input('starttime'),
                'statusservice' => $request->input('status_service')
            );            	 	            
            \DB::table('services_client')->insert($data);	 	
	 }

    function register()
    {

    }*/



    function sendmsj(){
        $body = $_GET['body'];
        $title = $_GET['title'];
        $registrationIds = $_GET['token_registration']; 
#API access key from Google API's Console
    define( 'API_ACCESS_KEY', 'AAAAMP3zBhM:APA91bHMs6Eil_O4gR4iSRqVg6m1oZ1Sxj5BmAW3bVScSc7iVIM5mxi4HUiS4zwNNtlOPDuK9TkHNuUJAkLlc5Fiz6xBiCSnuxQBU7UQNI7NlkI-y_11wj_wzSeKqSirzUQKgxvmFApa' );
   
#prep the bundle
     $msg = array
          (
        'body'  => $body,
        'title' =>  $title,
                'icon'  => 'myicon',/*Default Icon*/
                'sound' => 'mySound'/*Default sound*/
          );
    $fields = array
            (
                'to'        => $registrationIds,
                'notification'  => $msg
            );
    
    
    $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );
#Send Reponse To FireBase Server    
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
#Echo Result Of FireBase Server
echo $result;
    }

}
 