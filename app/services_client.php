<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class services_client extends Model
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'startdate', 'starttime', 'status_service', 'servicetype' 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
}
