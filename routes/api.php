<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Task;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {   
    return $request->user();
});

Route::get('posts', function(){
	return App\Post::all();
})/*->middleware('auth:api')*/;
/*************prueba del hash********************/
Route::get('pruebahash', function(Request $request){             
    //se llama a la vista que sirve para crear los servicios
    $llega="123456";    
    $correo="zambrano.jhonny@gmail.com";
    $sql="select password from users where email='".$correo."'";
    $resultado = \DB::select($sql);   
    $contra_actual=$resultado[0]->password;
    $resul=password_verify($llega,$contra_actual);
    if($resul==1)
        echo "lo que llega es igual a lo de la tabla";
    else
        echo "no coincide lo que llegó con la tabla";            
});
/***********************************************/
Route::post('crearservicios', function(Request $request){             
    //se llama a la vista que sirve para crear los servicios
   
    $sql="select * from services";
     $resultado = \DB::select($sql);        
      //ahora se llena el arreglo con los datos y se pasa a la vista
      $data=array();              
        foreach ($resultado as $key) {
            array_push($data ,  array(
            "id"=>$key->id,
            "description"=>$key->description
         
            ));
        }                       
    
    if(count($resultado)>0)
    {           
        return view('crearservicios')->with('servicios', $data);        
    }    
   // return View::make('crearservicios');    

});
Route::post('reprogramarservicios', function(Request $request){             
    //se llama a la vista que sirve para crear los servicios
   
    $sql="select * from motivoscancelacion";
     $resultado = \DB::select($sql);        
      //ahora se llena el arreglo con los datos y se pasa a la vista
      $data=array();              
        foreach ($resultado as $key) {
            array_push($data ,  array(
            "id"=>$key->id,
            "description"=>$key->description
         
            ));
        }                       
    
    if(count($resultado)>0)
    {           
        return view('reprogramarservicios')->with('servicios', $data);        
    }    
   // return View::make('crearservicios');    

});
Route::get('servicios', function(Request $request){         
   //se traen todos los servicios activos y cuyo id_roll pertenezca a un cliente   
    $sql="select users.Name, users.Last_name, users.id from users inner join services_client on services_client.id_user=users.id and services_client.status_service=0 group by users.id";
      $resultado = \DB::select($sql);        
      //ahora se llena el arreglo con los datos y se pasa a la vista
      $data=array();              
        foreach ($resultado as $key) {
            array_push($data ,  array(
            "id_user"=>$key->id,
            "name"=>$key->Name,
            "Last_name"=>$key->Last_name            
            ));
        }                       
    
    if(count($resultado)>0)
    {
        //ahora se llama la vista y se pasa la información                 
        return View::make('services')->with('datos', $data);
    }    

});
Route::post('verificaremail', function(Request $request){     
    $email=$request->input('email');    
    $sql="select * from users where Email='".$email."'";
   
    $resultado = \DB::select($sql);
    if(count($resultado)>0) //existe ese email
        return json_encode(array('existe' => 'si'));
    else
        return json_encode(array('existe' => 'no'));
});
Route::post('register', function(Request $request){     
 $v = Validator::make(
    array(
         'Name' => $request->input('name'),
                'Last_name' => $request->input('lastname'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'business' => $request->input('business'),
                'telephone' => $request->input('telephone'),
                'address' => $request->input('address')
        ), [
            'Name' => 'required|string|max:255',
            'Last_name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'password' => 'required|string|max:255',            
            'password_confirmation' => 'same:password',
            'business' => 'required|string|max:255',
            'telephone' => 'required|string|max:255',
            'address' => 'required|string|max:255',
        ]);
        
        if ($v->fails() || $request->input('password') != $request->input('password_confirmation')) {             
            if($request->input('esionic') == "0")
            {
                return redirect()->route('register', ['error' => 0]);            
            } 
            if($request->input('esionic') == "1")
            {
                return json_encode(array('error' => '0'));
            }
        }else{             
            App\User::create([
                'Name' => $request->input('name'),
                'Last_name' => $request->input('lastname'),
                'user'=>" ",            
                'email' => $request->input('email'),
                'create_date'=>date("Y-m-d H:i:s"),
                'stat'=>0,
                'id_roll_user' => $request->input('id_roll_user'),
                'password' => bcrypt($request->input('password')),                
                'business' => $request->input('business'),
                'telephone' => $request->input('telephone'),
                'address' => $request->input('address'),
                'typeuser'=>$request->input('typeuser')
                
            ]); 

            if($request->input('esionic') == "0")
            {
                return redirect()->route('register', ['error' => 0]);            
            } 
            if($request->input('esionic') == "1")
            {
                return json_encode(array('error' => '1'));
            }

        }
 
});
Route::post('solicitarservicio', function(Request $request){   
    
    $data=array(
        "startdate"=>$request->input('startdate'),
        "starttime"=>$request->input('starttime'),        
        "created_at" =>  date("Y-m-d H:i:s"),
        "updated_at" =>  date("Y-m-d H:i:s"),
        "status_service"=>$request->input('status_service'),
        "servicetype"=>$request->input('servicetype'),
        "id_user"=>$request->input('id_user'),
        "lat"=>$request->input('lat'),
        "longitud"=>$request->input('long'),
        "observacion"=>$request->input('observacion')
         
    );    
    \DB::table('services_client')->insert($data);
    return json_encode(array('error' => '1'));

});
Route::post('reprogramarservicio', function(Request $request){   
    
    $startdate=$request->input('startdate');
    $starttime=$request->input('starttime');            
    $updated_at =date("Y-m-d H:i:s");        
    //$id_user=$request->input('id_user');
    $lat=$request->input('lat');
    $longitud=$request->input('long');
    $observacion=$request->input('observacion');
    $id_servicio=$request->input('id');


    $sql="update services_client set startdate='".$startdate."', starttime='".$starttime."', lat=$lat, longitud=$longitud,observacion='".$observacion."' where id=$id_servicio";   
    $resultado = \DB::select($sql); 
    return json_encode(array('error' => '1'));

});
Route::post('obtenerid', function(Request $request){             
    $email=$request->input('username');

    $sql="select * from users where email='$email'";
     $resultado = \DB::select($sql);
   $data = array();     
        foreach ($resultado as $key) {
            array_push($data ,  array(
            "id_user"=>$key->id,
            "typeuser"=>$key->typeuser,
            "nameuser"=>$key->Name." ".$key->Last_name                       
            ));    
        }                                   
    return json_encode($data);    
});
//traer motivos
Route::post('traermotivos', function(Request $request){    
   
    $sql="select * from events";
   $resultado = \DB::select($sql);  
   $data = array() ;
   
   $con=1;    
   $desc="";
        foreach ($resultado as $key) {                        
            array_push($data ,  array(            
            "id_evento"=>$key->id,
            "evento_nombre"=>$key->description,                        
            ));    
            
        }          
        
    return json_encode($data);
   

});
//eventos para la fase
Route::post('traereventosfase', function(Request $request){    
    $id_servicio=$request->input('id_servicio');
    $numero_fase=$request->input('numero_fase');
    $sql="select * from eventsservice  where id_service=$id_servicio and status_service=$numero_fase";
   $resultado = \DB::select($sql);  
   $data = array() ;
   
   $con=1;    
   $desc="";
        foreach ($resultado as $key) {                        
            array_push($data ,  array(            
            "name_event"=>$key->event_name,
            "event_description"=>$key->event_description,            
            "number"=>$con
            ));    
            $con++;
        }          
        
    return json_encode($data);
   

});
//trae los registros de movimientos por fase
Route::post('traerregmov', function(Request $request){        
    $id_servicio=$request->input('id_servicio');
    $fase=$request->input('numero_fase');
    $latitud=$request->input('latitud');
    $longitud=$request->input('longitud');
    $iniciatermina=$request->input('iniciatermina');
    
    if($iniciatermina=="inicia")
    {
            $sql="select * from movservice where id_service=$id_servicio and fase=$fase and startorend=1";
            $resultado = \DB::select($sql);  
            if(count($resultado)==0 && $iniciatermina="inicia") //siginfica que se está buscando para un inicio de fase
            {
                //se registra
                 $fields=array(
                    "id_service"=>$id_servicio,
                    "fase"=>$fase,
                    "latitud"=>$latitud,
                    "longitud"=>$longitud,
                    "datestaend"=>date("Y/m/d H:i:s"),
                    "timestaend"=>date("Y/m/d H:i:s"),
                    "startorend"=>1 //inicio
                );
                \DB::table('movservice')->insert($fields);                
                $data=array(
                        "seregistro"=>"si"
                );

            }    
            if(count($resultado)>0 && $iniciatermina="inicia" )//ya está registrado la parte de inicio
            {
                $data=array(
                        "seregistro"=>"no"
                );
            }           
    }
    else
    {
         $sql="select * from movservice where id_service=$id_servicio and fase=$fase and startorend=2";
        $resultado = \DB::select($sql);     
        if(count($resultado)==0 && $iniciatermina="termina") //siginfica que se está buscando para un inicio de fase
            {
                //se registra
                 $hoy = getdate();
                 $fields=array(
                    "id_service"=>$id_servicio,
                    "fase"=>$fase,
                    "latitud"=>$latitud,
                    "longitud"=>$longitud,
                    "datestaend"=>date("Y/m/d H:i:s"),
                    "timestaend"=>date("Y/m/d H:i:s"),
                    "startorend"=>2 //termina
                );
                
                \DB::table('movservice')->insert($fields);                
                $data=array(
                        "seregistro"=>$hoy
                );

            }    
            if(count($resultado)>0 && $iniciatermina="termina" )//ya está registrado la parte de inicio
            {
                $data=array(
                        "seregistro"=>"no"
                );
            }    


    }
    

    

    
 
        
    return json_encode($sql);   
});
//-------------------------------
//traer servicios para el operario
Route::post('traerserviciosparaoperario', function(Request $request){        
    $idoperador=$request->input('user_id');
    $sql="select services_client.id as id_servicio,idoperador,u.Name, u.Last_name,servicetype, description, u.typeuser,id_user,startdate,status_service,lat,longitud,observacion from services_client inner join services inner join users as u on services.id=services_client.servicetype and services_client.status_service!=0 and u.id=id_user and idoperador=$idoperador and u.id=services_client.id_user order by startdate desc";
     $resultado = \DB::select($sql);     
    $sql2="select typeuser from users where id= $idoperador";
    $resultado2 = \DB::select($sql2);     
    foreach ($resultado2 as $key2) {     
        $tipo_usuario=$key2->typeuser;
    }
   $data = array(); 
   $con=1;    
   $desc="";
        foreach ($resultado as $key) {            
            if($key->status_service==1)
                $desc="Aprobado"; 
            if($key->status_service==2)
                $desc="Armado"; 
            if($key->status_service==3)
                $desc="Operación"; 
            if($key->status_service==4)
                $desc="Desarmado"; 
            array_push($data ,  array(
            "servicetype"=>$key->servicetype,
            "description"=>$key->description,
            "startdate"=>$key->startdate,
            "statusservice"=>$key->status_service, 
            "descstatusservice"=>$desc,                      
            "namelastname"=>$key->Name." ".$key->Last_name,
            "lat"=>$key->lat,
            "longitud"=>$key->longitud,
            "observacion"=>$key->observacion,   
            "id_servicio"=>$key->id_servicio, 
            "idoperador"=>$key->idoperador,  
            "tipo_usuario"=>$tipo_usuario,      
            "number"=>$con
            ));    
            $con++;
        }                  
    return json_encode($data);   
});
//servicios culominados para el cliente
Route::post('traerserviciosusuarioculminados', function(Request $request){                    
    $iduser=$request->input('user_id');    
    $sql="select services_client.id as id_servicio,lat,longitud,observacion,servicetype, description, id_user,startdate,status_service from services_client inner join services  on services.id=services_client.servicetype and services_client.id_user='$iduser' and services_client.status_service=5  order by startdate desc";   
     $resultado = \DB::select($sql);     
     
   $data=array(
        "usuario"=>$iduser
   );
   $data = array(); 
   $con=1;    
   $desc="";
        foreach ($resultado as $key) {
            if($key->status_service==0)
                $desc="No Aprobado";
            if($key->status_service==1)
                $desc="En Proceso";
            if($key->status_service==2)
                $desc="Aprobado";
            if($key->status_service==3)
                $desc="Operación"; 
            if($key->status_service==4)
                $desc="Desarmado"; 
            if($key->status_service==5)
                $desc="Finalizado"; 
            array_push($data ,  array(
                "namelastname"=>"",
            "servicetype"=>$key->servicetype,
            "description"=>$key->description,
            "startdate"=>$key->startdate,
            "statusservice"=>$key->status_service,              
            "descstatusservice"=>$desc, 
            "lat"=>$key->lat,
            "longitud"=>$key->longitud,
            "observacion"=>$key->observacion,   
            "id_servicio"=>$key->id_servicio,                       
            "number"=>$con,
            "usuario"=>$iduser,
           
           
            ));    
            $con++;
        }            
    return json_encode($data);   
});
//------------------------------------------
//traer servicios por usuario
Route::post('traerserviciosusuario', function(Request $request){                    
    $iduser=$request->input('user_id');    
    $sql="select users.name, users.Last_name,services_client.id as id_servicio,lat,longitud,observacion,servicetype, description, id_user,startdate,status_service from services_client inner join services inner join users on services.id=services_client.servicetype and services_client.id_user='$iduser' and services_client.status_service!=0 and services_client.status_service!=5 and  idoperador=users.id order by startdate desc";

    /* $Sql="select users.name, users.Last_name,services_client.id as id_servicio,idoperador,lat,longitud,observacion,servicetype, description, id_user,startdate,status_service from services_client inner join services inner join users  on services.id=services_client.servicetype and services_client.id_user='$iduser' and services_client.status_service=5 and idoperador!=0 and  idoperador=users.id order by startdate desc";*/
     $resultado = \DB::select($sql);   
     $sql2="select typeuser from users where id=$iduser";
     $resultado2 = \DB::select($sql2);   
     foreach ($resultado2 as $key2) {
            $tipousuario=$key2->typeuser;
     }
   $data=array(
        "usuario"=>$iduser
   );
   $data = array(); 
   $con=1;    
   $desc="";
        foreach ($resultado as $key) {
            if($key->status_service==0)
                $desc="No Aprobado";
            if($key->status_service==1)
                $desc="En Proceso";
            if($key->status_service==2)
                $desc="Aprobado";
            if($key->status_service==3)
                $desc="Operación"; 
            if($key->status_service==4)
                $desc="Desarmado"; 
            array_push($data ,  array(
                "namelastname"=>"",
            "servicetype"=>$key->servicetype,
            "description"=>$key->description,
            "startdate"=>$key->startdate,
            "statusservice"=>$key->status_service,              
            "descstatusservice"=>$desc, 
            "lat"=>$key->lat,
            "longitud"=>$key->longitud,
            "observacion"=>$key->observacion,   
            "id_servicio"=>$key->id_servicio,                       
            "number"=>$con,
            "usuario"=>$iduser,
            "operador"=>$key->name." ".$key->Last_name,
            "tipo_usuario"=>$tipousuario
            ));    
            $con++;
        }            
    return json_encode($data);   
});
Route::post('ultimafase', function(Request $request){   
$id_servicio=$request->input('id_servicio');
$calificacion=$request->input('calificacion');
$comentario=$request->input('descripcion');
$update="update services_client set idsatisfaction=$calificacion,comentario = '".$comentario."' where id='$id_servicio'";  
    $return = \DB::select($update);     
    
 return json_encode(array('error' =>$update));

});
Route::post('cambiarfase', function(Request $request){   
$id_servicio=$request->input('id_servicio');
$fase_actual=$request->input('fase_actual');
$fase_actual++;

    $update="update services_client set status_service = '$fase_actual' where id='$id_servicio'";    
    $return = \DB::update($update);   
 return json_encode(array('error' =>$return));

});
Route::post('tokenregister', function(Request $request){   
$token = $request->input('token');
$id = $request->input('id');

    $update="update users set token_api = '$token' where id='$id'";    
    $return = \DB::update($update);   
 return json_encode(array('error' =>$return));

});
Route::get('getokenregister', function(Request $request){   
$id = $request->input('id');

    $sql="select token_api from users where id='$id'";    
    $select = \DB::select($sql);   
 return json_encode($select);

});
Route::post('subirfoto', function(Request $request){       
        $foto=$request->input('foto');
        $evento=$request->input('evento');
        $nombre_evento=$request->input('nombre_evento');
        $id_servicio=$request->input('id_servicio');
        $status_servicio=$request->input('status_servicio');
        //se crea la conuslta que inserta en la tabla
        
        $fields=array(
            "id_service"=>$id_servicio,
            "photo"=>$foto,
            "event_description"=>$evento,
            "status_service"=>$status_servicio,
            "event_name"=>$nombre_evento
        );
        \DB::table('eventsservice')->insert($fields);
        return json_encode(array('error' => '1'));

});
Route::post('serviciosparacliente', function(Request $request){       
        $idusuario=$request->input('iduser');
        $sql="select services_client.id,services_client.servicetype,services_client.id_user, services.description from services_client inner join services on services.id=services_client.servicetype and services_client.status_service=0 and services_client.id_user=$idusuario";               
    $resultado = \DB::select($sql);
    //$id = Auth::user()->id;
    $data = array();     
        foreach ($resultado as $key) {
            array_push($data ,  array(
            "id"=>$key->id,
            "description"=>$key->description
           // "id_user"=>$id 
            ));    
        }        
    return json_encode($data);

});
Route::get('motivoscancelacion', function(Request $request){
    $sql="select * from motivoscancelacion ";
    $resultado = \DB::select($sql);
    //$id = Auth::user()->id;
    $data = array();     
        foreach ($resultado as $key) {
            array_push($data ,  array(
            "id"=>$key->id,
            "description"=>$key->description,
           // "id_user"=>$id 
            ));    
        }        
    return json_encode($data);
    

});
Route::get('traertiposervicio', function(Request $request){
    $sql="select * from services ";
    $resultado = \DB::select($sql);
    //$id = Auth::user()->id;
    $data = array();     
        foreach ($resultado as $key) {
            array_push($data ,  array(
            "id"=>$key->id,
            "description"=>$key->description,
           // "id_user"=>$id 
            ));    
        }        
    return json_encode($data);
    

})->middleware('auth:api');


Route::post('verificaremail', function(Request $request){     
    $email=$request->input('email');    
    $sql="select * from users where Email='".$email."'";
   
    $resultado = \DB::select($sql);
    if(count($resultado)>0) //existe ese email
        return json_encode(array('existe' => 'si'));
    else
        return json_encode(array('existe' => 'no'));
});


//Route::post('/solicitarservicio', array( 'uses' => 'ServicioController@solicitarservicio' ) );
