<?php
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/servicios', array( 'uses' => 'ServicioController@serviciostodosclientes' ) ); 

Route::get('/regservicios', array( 'uses' => 'ServicioController@regservicios' ) );
Route::get('/regmotivo', array( 'uses' => 'ServicioController@regmotivos' ) );
Route::get('/regevento', array( 'uses' => 'ServicioController@regevento' ) );
       
   
 

Route::post('/editarservicio/{id}', array( 'uses' => 'ServicioController@editarservicio' ) );
Route::post('/editarmotivo/{id}', array( 'uses' => 'ServicioController@editarmotivo' ) );
Route::post('/editarevento/{id}', array( 'uses' => 'ServicioController@editarevento' ) );

Route::post('/eliservicio/{id}', array( 'uses' => 'ServicioController@eliminaservicio' ) );
Route::post('/elimotivo/{id}', array( 'uses' => 'ServicioController@eliminarmotivo' ) );
Route::post('/elievento/{id}', array( 'uses' => 'ServicioController@eliminarevento' ) );
Route::post('/mostrarservicioscliente/{id}', array( 'uses' => 'ServicioController@mostrarserviciosporcliente' ) );
Route::get('/actualizarservicios/{id_servicio}', array( 'uses' => 'ServicioController@actualizarservicioscliente' ) );
Route::post('/client', function () {
    return view('client');
})->middleware('auth');
Route::post('/solicitarservicio', array( 'uses' => 'ServicioController@solicitarservicio' ) );


Route::get('imagen', function(Request $request){             
//eliminamos data:image/png; y base64, de la cadena que tenemos
//hay otras formas de hacerlo                   
	$sql="select id,id_service,photo from eventsservice where photo!='empty'";
	 $resultado = \DB::select($sql);          
	 $contador=0;
	 foreach ($resultado as $key) {
	            $Base64Img="'.$key->photo.'";
	            list(, $Base64Img) = explode(';', $Base64Img);
				list(, $Base64Img) = explode(',', $Base64Img);
				$Base64Img = base64_decode($Base64Img);
				file_put_contents($key->id_service.'_'.$contador.'.png', $Base64Img);			
				//luego se cambia el campo photo por la cadena empty
				$sql="update eventsservice set photo='empty' where id=$key->id";
				 $resultado = \DB::select($sql);    
				$contador++;            				
	}  
});

Route::get('/sendmsj', array( 'uses' => 'ServicioController@sendmsj' ) );


Route::get('/aprobar', array( 'uses' => 'ServicioController@aprobar' ) );
Route::get('/eventos', array( 'uses' => 'ServicioController@eventos' ) );
Route::post('/aprobar', array( 'uses' => 'ServicioController@aprobar' ) );
