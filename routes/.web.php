<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/servicios', array( 'uses' => 'ServicioController@serviciostodosclientes' ) ); 

Route::post('/mostrarservicioscliente/{id}', array( 'uses' => 'ServicioController@mostrarserviciosporcliente' ) );
Route::get('/actualizarservicios/{id_servicio}', array( 'uses' => 'ServicioController@actualizarservicioscliente' ) );

/*Route::get('/mostrarservicioscliente', array( 'uses' => 'ServicioController@mostrarserviciosporcliente' ) );*/

Route::post('/client', function () {
    return view('client');
})->middleware('auth');
Route::post('/solicitarservicio', array( 'uses' => 'ServicioController@solicitarservicio' ) );
Route::get('/sendmsj', array( 'uses' => 'ServicioController@sendmsj' ) );
