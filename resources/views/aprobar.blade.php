@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Servicios Operador</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                           {{ session('status') }}
                        </div>
                    @endif 
                    <form class="form-horizontal" method="POST" action="{{ '/aprobar' }}">
 {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('typeuser') ? ' has-error' : '' }}">
                            <label for="typeuser" class="col-md-4 control-label">Operador</label>

                            <div class="col-md-6">  

                                <select class="selectpicker form-control" id="typeuser" name="typeuser" required >
                                    	<option value="" >Seleccione un operador</option>
                                	<?php foreach ($datos as $item) { ?>
                                    	<option value="<?=  $item['id'] ?>"><?=  $item['name'] ?></option>
                                	<?php } ?>
                                </select>
                                @if ($errors->has('typeuser'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('typeuser') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 

                        <div class="form-group{{ $errors->has('Servicios') ? ' has-error' : '' }}">
                            <label for="Servicios" class="col-md-4 control-label">Servicios</label>

                            <div class="col-md-6">  

                                <select multiple class="selectpicker form-control" id="servicios" name="servicios[] "  required> 
                                	<?php foreach ($datos2 as $item) { ?>
                                    	<option value="<?=  $item['id'] ?>"><?=  $item['observacion'] ?></option>
                                	<?php } ?>
                                </select>
                                @if ($errors->has('Servicios'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('Servicios') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 
 
                    <div class="row">
                        <div class="col-md-12 text-center">  
                        	<button type="submit" class="btn btn-primary">Enviar</button>   
                        </div>
					</div>

</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
