@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">Registro de Usuarios</div>
                            <?php if(isset($_GET["error"]) && $_GET["error"] == "0"){ ?>
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                            <?php } ?>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ '/api/register' }}">
 {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"  autofocus>

                                @if ($errors->has('name'))   
                                    <span class="help-block">
                                        <strong>..--{{ $errors->first('name') }}</strong>
                                    </span>
                                 @endif
                            </div>

                            <div class="col-md-6">
                                <input id="id_roll_user" type="hidden" class="form-control" name="id_roll_user" value="1"  autofocus>
                                <input id="esionic" type="hidden" class="form-control" name="esionic" value="0"  autofocus>

                                @if ($errors->has('id_roll_user'))   
                                    <span class="help-block">                                        
                                    </span>
                                 @endif
                            </div>

                        </div>

                        <!--lastname!-->
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Apellido</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}"    autofocus>

                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo Electrónico</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"   >

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password"   >

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmar Contraseña</label>

                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation"   >
                            </div>
                        </div>
                        <!--business, telephone, address!-->
                         <div class="form-group{{ $errors->has('business') ? ' has-error' : '' }}">
                            <label for="business" class="col-md-4 control-label">Empresa</label>

                            <div class="col-md-6">
                                <input id="business" type="text" class="form-control" name="business" value="{{ old('business') }}"    autofocus>

                                @if ($errors->has('business'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!--telehone!-->
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="telephone" class="col-md-4 control-label">Teléfono</label>

                            <div class="col-md-6">
                                <input id="telephone" type="text" class="form-control" name="telephone" value="{{ old('telephone') }}"    autofocus>

                                @if ($errors->has('telephone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telephone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!--address!-->
                          <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Dirección</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}"    autofocus>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!--type of user!-->
                         <div class="form-group{{ $errors->has('typeuser') ? ' has-error' : '' }}">
                            <label for="typeuser" class="col-md-4 control-label">Tipo de Usuario</label>

                            <div class="col-md-6">                                
                                <select class="selectpicker form-control" id="typeuser" name="typeuser" autofocus>
                                    <option value="1">Cliente</option>
                                    <option value="2">Operario</option>
                                    <option value="3">Administrador</option>
                                </select>
                                @if ($errors->has('typeuser'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('typeuser') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!------------------------!-->
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
