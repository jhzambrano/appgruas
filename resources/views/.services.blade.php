@extends('layouts.app')
@section('content')
<head>
  <meta charset="utf-8">  
  <title>event.preventDefault demo</title>
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
<script>
  // Initialize Firebase
  // TODO: Replace with your project's customized code snippet
  var config = {
    apiKey: "<API_KEY>",
    authDomain: "<PROJECT_ID>.firebaseapp.com",
    databaseURL: "https://<DATABASE_NAME>.firebaseio.com",
    storageBucket: "<BUCKET>.appspot.com",
    messagingSenderId: "<SENDER_ID>",
  };
  firebase.initializeApp(config);
</script>
<script>
   /**/
   $(function() {    
         $('#myModal').modal('hide');
         $(document).on('click', 'input[type="button"]', function(event) {
            let id = this.id;     
            var data=this.id;       
            //console.log("Se presionó el Boton con Id :"+ id)
            //se llama la ruta de laravel
            
              var token = $('input[name=_token]').val();
               $.ajax({
                    type: "post",
                    url: "/mostrarservicioscliente/"+id,
                    data: {
                        'id': data,
                        'tok': token
                    },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {    
                                        
                        //$('#myModal').modal('show');
                        $("#titulo_modal").html("Servicios Pendientes Para: "+(data[0].name+" "+data[0].Last_name).toUpperCase());
                       
                        var tabla="<table class='table'><thead><tr><th>#Servicio</th><th>Nombre Servicio</th><th>Fecha Inicio</th><th>Hora Inicio</th><th>Aprobado por:</th><th>Aprobar</th></tr></thead>";
                        
                        //se cuentan los registroa traidos
                        for(i=0;i<data.length;i++)
                        {
                                tabla+="<tbody><tr><td>";
                                tabla+=data[i].id_service;
                                tabla+="</td><td>";
                                tabla+=data[i].description;
                                tabla+="</td><td>"
                                tabla+=data[i].startdate;
                                tabla+="</td><td>";
                                tabla+=data[i].starttime;
                                tabla+="</td><td><select class='form-control' name='apr'><option value='0'>Razon 1</option><option value='1'>Razon 2</option></select></td>";
                                tabla+="<td><div class='checkbox'><label><input data-token='"+data[i].token_api+"' type='checkbox' value='"+data[i].id_service+"'>";             
                                tabla+="</label></div></td></tr></tbody>";
                        }
                        tabla+="</table>";
                        $("#cuerpo_modal").html(tabla);
                        $('#myModal').appendTo("body").modal('show');
                        $("#guardar").click(function(){
                            var arreglo=[];    
                            arreglo.length=0;
                                //se envian los datos al controlador para guardar
                                $('input[type=checkbox]:checked').each(function() {
                                   /*
                                    $.ajax({
                                        type: "get",
                                        url: "/actualizarservicios/"+$(this).val(), 
                                         data: {
                                                'id_servicio': $(this).val()     
                                        },                                   
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        success: function(data) {
                                            
                                        }
                                    });*/
                                    //------------------------------------------------------------
                                    var to = $(this).data( "token" );
                                    $.ajax({
                                        type: "get",
                                        url: "/sendmsj",  
                                        data: {body: 'cuerpo', title:'title', token_registration:to},
                                        success: function(data) {
                                        }
                                    });
                                    //------------------------------------------------------------
                                });   
                                
                                //se llama al metodo que gaurda en la tabla

                        }); //fin guardar
                    },
                    error: function(data){
                        alert("fail");
                    }
                });

          });
    });
</script>
<!--modal!-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 id="titulo_modal" class="modal-title">Servicios Activos para</h4>
        </div>
        <div class="modal-body" id="cuerpo_modal">
          <p></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="guardar">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<!-------------------------!-->
<div class="container">
    <div class="row">
        
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Servicios Pendientes</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                   <form class="form-horizontal" method="POST" action="">
                    <input type="hidden" id="_token" value="{{ csrf_token() }}">
                        
                            
                        
                        <table class="table">
                          <thead>
                            <tr>
                              <th>Id Usuario</th>
                              <th>Nombre</th>
                              <th>Apellido</th>
                              <th>Servicios Pendientes</th>
                            </tr>
                          </thead>
                          
                          <tbody>
                            
                            <!--foreach!-->
                            @foreach($datos as $item)
                                 <tr>
                                    
                                    <th scope="row">{{$item['id_user']}}</th>
                                    <td>{{$item['name']}}</td>
                                    <td>{{$item['Last_name']}}</td>
                                    
                                    <td>
                                        
                                        <input type="button" name="e" value="Ver" id="{{$item['id_user']}}" class="btn btn-primary">
                                    </td>
                                    <!--<td><a href="{{ url('api/mostrar_servicios', $item['id_user']) }}">Edit</a></td>!-->
                                </tr>                                
                            @endforeach     
                          </tbody>

                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
