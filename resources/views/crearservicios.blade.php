@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
  {{-- bootstrap css CDN --}}
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  {{-- bootstrap js CDN --}}
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <title>Todo List App</title>
</head>
<script>
    $(function() {  

        $('#myModal').modal('hide');
         var datai=""
         var ingresar="false";
         $(document).on('click', 'input[type="button"]', function(event) {
         var value_input = $("input[name*='e']").val();
         
            datai=this.id;
          if(this.name!="elimina"){



            if(ingresar=="false")
            {
                    let id = this.id;                
                    datai=this.id;
                 
                        var tabla="";
                        tabla="<form><div class='form-group'><label for='email'>Nueva Descripción:</label><input type='text' class='form-control' id='nueva' name='nueva'></div></form>";

                        $("#cuerpo_modal").html(tabla);
                        $('#myModal').modal('show');     
            }
            

             }
           else   
            {
              //aqui se llama por ajax el metodo que eliminaría el servicio

              $.ajax({
                      type: "post",
                      url: "/eliservicio/"+datai, 
                     //data:"id_servicio="+data,
                      data:{
                         
                          id: datai
                      },                               
                      
                      dataType: "json",
                      success: function(data) {                          
                          if(data.puedeeliminar=="si")
                            location.reload();
                          else
                            alert("el servicio no se puede eliminar porque hay clientes que ya lo tienen asociado");
                      }
                  });  
            }  
          });
            $("#agregar").click(function(){
               ingresar="true";             
                  $.ajax({
                                        type: "get",
                                        url: "/regservicios", 
                                       //data:"id_servicio="+data,
                                        data:{
                                           
                                            description: $("#newTaskName").val()
                                        },                               
                                        
                                        dataType: "json",
                                        success: function(data) {
                                          
                                          location.reload();
                                        }
                                    });  
               
            });
            $("#guardar").click(function(){                     
                                //se envian los datos al controlador para guardar                               
                                  
                                    $.ajax({
                                        type: "post",
                                        url: "/editarservicio/"+datai, 
                                       //data:"id_servicio="+data,
                                        data:{
                                            id:datai,
                                            description: $("#nueva").val()
                                        },                               
                                        
                                        dataType: "json",
                                        success: function(data) {
                                            
                                            //se redirecciona a la misma ruta
                                         //   window.location.href = "{{url('/api/crearservicios')}}";
                                          location.reload();
                                        }
                                    });
                                    //---------------------------------------------
                                    //------------------------------------------------------------
                                 
                                
                                //se llama al metodo que gaurda en la tabla

            }); //fin guardar 

        });
</script>
<body>
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 id="titulo_modal" class="modal-title">Editar Servicio</h4>
        </div>
        <div class="modal-body" id="cuerpo_modal">
          <p></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="guardar">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
<div class="container">
  <div class="col-md-offset-2 col-md-8">
    <div class="row">
      <h1>Servicios</h1>
    </div>

    

    <div class="row" style='margin-top: 10px; margin-bottom: 10px;'>
      <form action="{{ url('/api/regservicios') }}" method='POST'>
      {{ csrf_field() }}

        <div class="col-md-9">
          <input type="text" name='newTaskName' id="newTaskName" class='form-control'>
        </div>

        <div class="col-md-3">
          <input type="button" id="agregar" class='btn btn-primary btn-block' value='Agregar Servicio'>
        </div>
      </form>
    </div>

    {{-- display stored tasks --}}
    @if (count($servicios) > 0)
      <table class="table">
        <thead>
          <th>Servicio #</th>
          <th>Descripcion</th>
          
          
        </thead>

        <tbody>
                       
          @foreach ($servicios as $servicio)
            <tr>
              <th>{{$servicio['id']}}</th>
              <td>{{ $servicio['description'] }}</td>
              <td> <input type="button" name="e" value="Editar" id="{{$servicio['id']}}" class="btn btn-primary"></td>
              <td> <input type="button" name="elimina" value="Eliminar" id="{{$servicio['id']}}" class="btn btn-primary"></td>
              
            </tr>
          @endforeach
          <tr>
            <th></th>
            <th></th>
            <th><a href="{{url('/home')}}">Regresar</a> &nbsp; 

                  

            </th>
            
        </tr>
        </tbody>
         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
      </table>
    @endif

    <div class="row text-center">
      
    </div>

  </div>
</div>
</body>
</html>
@endsection 