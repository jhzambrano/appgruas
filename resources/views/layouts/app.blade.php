<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Gruas</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        Gruas
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Acceder</a></li>
                            <li><a href="{{ route('register') }}">Registrarse</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->Name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <!-- Servicios Operador !-->
                                     <li>
                                        <a href="{{ url('/aprobar') }}">
                                           Servicios Operador
                                        </a>

                                       
                                    </li>
                                    <!--Registrar eventos!-->
                                    <li>
                                        <a href="{{ url('/eventos') }}">
                                           Eventos para Servicios
                                        </a>

                                       
                                    </li>

                                     <!--crear servicios !-->
                                     <li>
                                        <a href="{{ url('/api/crearservicios') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form2').submit();">
                                           Crear/Actualizar Servicios
                                        </a>

                                        <form id="logout-form2" action=" {{ url('/api/crearservicios') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                       
                                    </li>
                                    <!--motivos para reprogramar servicio!-->
                                    <li>
                                        <a href="{{ url('/api/reprogramarservicios') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form3').submit();">
                                           Motivos/Reprogramar Servicios
                                        </a>

                                        <form id="logout-form3" action=" {{ url('/api/reprogramarservicios') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                       
                                    </li>
                                    <!--cerrar sesion!-->
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                </ul>

                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
